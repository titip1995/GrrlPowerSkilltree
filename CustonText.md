# Custom Text System.

This is the documentation of the custom text engine.

## Text Examples.

### Example 1: Links and Super-Script
```
{c:[
    {
      k:"a",
      p:'510'
    },
    {
      k:"sup",
      c:{
        c:[
          "revealed",
          {
            k:"a",
            p:'659'
          }
        ]
      }
    },
  ]}
```   
GENERATES:   
```
<a href="http://grrlpowercomic.com/archives/2457" title="#510 - Maybe it’s a cupcake beam">#510</a>
<sup> revealed
  <a href="http://grrlpowercomic.com/archives/2942" title="#659 - The greatest challenge – upgrading">#659</a>
</sup>
```  
In this case the links are automatically generated. The generator defined in `onNodeClick.js`.

### Example 2: Unordered List

```
{
  k:"ul",
  c:[
    "Cutting Beam Weapon",
    "Scatter / Rapid Fire Beam Weapon"
  ]
}
```   
GENERATES:
```
<ul>
  <li>Cutting Beam Weapon</li>
  <li>Scatter / Rapid Fire Beam Weapon</li>
</ul>
```

### Example 3: Concentrating Text & Links

```
{
  k:"t",
  c:[
    "Provides speed up to Mk. 4. (1372 m/s)",
    {
      k: "sup",
      c: {
        k: "a",
        p: "670"
      }
    }
  ]
}
```   
GENERATES:   
```
Provides speed up to Mk. 4. (1372 m/s)
<sup>
  <a href="http://grrlpowercomic.com/archives/2978" title="#670 - Orbital bombardment obstacle course">#670</a>
</sup>
```  
In this case the link is automatically generated. The generator defined in `onNodeClick.js`.

### Example 4: Manual Line Breaks

```
{
  k:"t",
  c:[
    "Provides speed up to Mk. 4. (1372 m/s)",
    "<br />",
    "In most cases we don't use line breaks, we use unordered lists."
  ]
}
```   
GENERATES:   
```
Provides speed up to Mk. 4. (1372 m/s) <br />
In most cases we don't use line breaks, we use unordered lists.
```  
`<br />` is a HTML line-break. I personally frown upon it.


# Format

Each tag must be in the following format:  
`{k:TYPE, c:[ITEM, ITEM, ITEM]}`  
Where type is the type of content (If left out defaults to `t`), and ITEM are ether strings or more objects in the same format.

# Text Processing Loop

* Check if input is a string.
* Check if the object has a `k` property.
  * If no tag, treat as if it had a `t` tag
  * Otherwise process said type.
* Process each `c` item using a new Text Processing Loop

# Types (`k` property)

* undefined: treat as `t`.
* `t`: Concentrate Text.
* `ul`: Unordered list.
* `sup`: superscript.
* `a`: links

## `t` type

`t` tag just concentrates the content

## `ul` type

`ul` tag makes the content into a unordered list. Each item is

## `sup` type

`sup` tag makes the content superscripted.

## `a` type
**this tag ignores the `c` property**

`a` tag makes links.

* If there is a property `p` it will use a previously defined `linkModderFunction` to generate the link.
* Otherwise it will use the `l` property as the link and the `t` property as the inner text.

### the linkModderFunction that is used by `onNodeClick.js`.

If you set p to a *comic number* it will auto-generate the url.

The comic id is not what you see in the URL, it's what you see on the title of the comic page.  
IE: `Grrl Power #651 – Obtuse loop` (651 is the comic number in this case)
