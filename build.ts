import { emptyDir, walk, copy, ensureDir } from "https://deno.land/std/fs/mod.ts";
import { Language, minify, minifyHTML } from "https://deno.land/x/minifier/mod.ts";
import { saveComics } from "./generators/comicIndex.ts";

const HTMLTest = /.*\.html/;
const CSSTest = /.*\.css/;

{
  const path = /static\//;
  await emptyDir("./public");
  for await (const entry of walk("./static")){
    if(entry.isDirectory){
      continue;
    }
    const writePath = entry.path.replace(path, "public/");
    if(HTMLTest.test(entry.name)){
      const code = await Deno.readTextFile(entry.path);
      //Triggering some unreachable error...
      //await Deno.writeTextFile(writePath, minify(Language.HTML, code));
      // const minified = await minifyHTML(code, {
      //   minifyCSS: true,
      //   minifyJS: true,
      // });
      // await Deno.writeTextFile(writePath, minified);
      await Deno.writeTextFile(writePath, code);
      continue;
    }
    if(CSSTest.test(entry.name)){
      const code = await Deno.readTextFile(entry.path);
      //await Deno.writeTextFile(writePath, minify(Language.CSS, code));
      await Deno.writeTextFile(writePath, code);
      continue;
    }
    await ensureDir(writePath.replace(entry.name, ""));
    await copy(entry.path, writePath, {overwrite: true});
  }
  const { files, diagnostics } = await Deno.emit(
    `./src/orbMap.ts`,
    {
      bundle: "esm"
    }
  )

  if (diagnostics.length) {
    // there is something that impacted the emit
    console.warn(Deno.formatDiagnostics(diagnostics));
  }
  //Deno.writeTextFile("./public/orbMap.js", minify(Language.JS ,files["deno:///bundle.js"]));
  await ensureDir("./public/js/");
  Deno.writeTextFile("./public/js/orbMap.js", files["deno:///bundle.js"]);
  await saveComics();
}

console.log("Bundling is done!")