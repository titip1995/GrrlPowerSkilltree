var nodeGroupStyles = {
    UnAvailable: {
        shape: 'dot',
        color: {
            background: '#9300ff',
            border: '#750099'
        },
        font: {
            color: 'black',
            size: 30
        },
        size: 20
    },
    Available: {
        shape: 'dot',
        color: {
            background: '#FE0000',
            border: '#800000'
        },
        font: {
            color: 'black',
            size: 30
        },
        size: 20
    },
    Purchased: {
        shape: 'dot',
        color: {
            background: '#007F0E',
            border: '#073B0B'
        },
        font: {
            color: 'black',
            size: 30
        },
        size: 20
    },
    On: {
        shape: 'dot',
        color: {
            background: '#007F0E',
            border: '#073B0B'
        },
        font: {
            color: 'black',
            size: 30
        },
        size: 20
    },
    Off: {
        shape: 'dot',
        color: {
            background: '#FE0000',
            border: '#800000'
        },
        font: {
            color: 'black',
            size: 30
        },
        size: 20
    },
    Disabled: {
        shape: 'dot',
        color: {
            background: '#FF00DC',
            border: '#4E0042'
        },
        font: {
            color: 'black',
            size: 30
        },
        size: 20
    },
    VanishingPoint: {
        shape: 'dot',
        color: {
            background: 'gray',
            border: 'darkgray'
        },
        font: {
            color: 'black',
            size: 30
        },
        size: 20
    },
    Hidden: {
        shape: 'dot',
        color: {
            background: 'white',
            border: 'white'
        },
        font: {
            color: 'black',
            size: 30
        },
        size: 20
    },
    Orb: {
        shape: 'circularImage',
        size: 50,
        font: {
            color: 'black',
            size: '35',
            background: '#FFFFFF80'
        }
    },
    TrueCenter: {
        image: './images/Orbs/PizzaPie.png',
        shape: 'image',
        color: {
            background: '#FFFFFF80',
            border: '#FFFFFF80'
        },
        shapeProperties: {
            useBorderWithImage: true
        },
        size: 40
    }
};
class Orb {
    constructor(id, group1, x, y, label, image, data1, commentBorder1){
        this.id = id.toUpperCase();
        this.group = group1;
        if (label) {
            this.label = label;
        }
        if (image) {
            this.image = image;
        }
        var tempFixed = {
        };
        if (x) {
            this.x = x;
            if (x == 'zero') {
                this.x = 0;
            }
            tempFixed.x = true;
        }
        if (y) {
            this.y = y;
            if (y == 'zero') {
                this.y = 0;
            }
            tempFixed.y = true;
        }
        if (x || y) {
            this.fixed = tempFixed;
        }
        var localData = data1 || {
        };
        if (data1 || this.id !== id) {
            var tempData = data1 || {
            };
            if (tempData.notes) {
                tempData.nodeNote = true;
            }
            if (tempData.unlocked) {
                tempData.nodeUnlockedBasic = true;
            }
            if (tempData.ull) {
                tempData.nodeUnlockedLink = true;
            }
            if (tempData.whatItDoes) {
                tempData.nodeWhatItDoes = true;
            }
            if (this.id !== id) {
                tempData.beautifulID = id;
            }
            this.colorModder(group1, commentBorder1, tempData);
            this.data = tempData;
        }
    }
    colorModder(group, commentBorder, data) {
        var bg = 'white';
        var bdr = 'white';
        var comment = 'gold';
        if (nodeGroupStyles[group].color) {
            if (nodeGroupStyles[group].color.background) {
                bg = nodeGroupStyles[group].color.background;
            }
            if (nodeGroupStyles[group].color.border) {
                bdr = nodeGroupStyles[group].color.border;
            }
        }
        var changed = false;
        var borderWidth = false;
        if (data.nodeUnlockedLink) {
            bg = '#00CC84';
            comment = 'orange';
            changed = true;
        }
        if (data.nodeNote) {
            if (commentBorder) {
                if (commentBorder == "On") {
                    bdr = comment;
                    changed = true;
                    borderWidth = true;
                } else {
                    changed = false;
                    borderWidth = false;
                }
            } else {
                var skipThese = [
                    "Hidden",
                    "Orb",
                    "TrueCenter"
                ];
                if (skipThese.indexOf(group) == -1) {
                    bdr = comment;
                    changed = true;
                    borderWidth = true;
                }
            }
        }
        if (changed) {
            this.color = {
                border: bdr,
                background: bg
            };
        } else {
            delete this.color;
        }
        if (borderWidth) {
            this.borderWidth = 3;
        } else {
            delete this.borderWidth;
            delete this.size;
        }
    }
}
function orb(id1, group2, x1, y1, label1, image1, data2, commentBorder2) {
    return new Orb(id1, group2, x1, y1, label1, image1, data2, commentBorder2);
}
class Edge {
    constructor(from, to, status1, dashed, arrow, straight, length){
        this.from = from.toUpperCase();
        this.to = to.toUpperCase();
        this.width = 4;
        if (status1 == "En") {
            this.color = {
                color: '#0125FF',
                highlight: '#0125FF',
                inherit: false
            };
        } else if (status1 == "Dis") {
            this.color = {
                color: '#3CC2E0',
                highlight: '#3CC2E0',
                inherit: false
            };
        } else if (status1 == "Invs") {
            this.color = {
                color: 'silver',
                highlight: 'silver',
                inherit: false
            };
            this.hidden = true;
        }
        if (dashed) {
            this.dashes = [
                24,
                8,
                8,
                8
            ];
        }
        if (arrow) {
            this.arrows = 'to';
        }
        if (straight) {
            this.smooth = false;
        }
        if (length) {
            if (length == "auto") {
            } else {
                this.length = length;
            }
        } else {
            this.length = 30;
        }
    }
}
function edge(from1, to1, status1, dashed1, arrow1, straight1, length1) {
    return new Edge(from1, to1, status1, dashed1, arrow1, straight1, length1);
}
var comOrbNodes = [
    orb('CO', 'Orb', 640, 140, 'Com-Ball / Communications Orb', './images/Orbs/ComOrb.png', {
        status: "The Com-Orb",
        notes: {
            k: "ul",
            c: [
                "One unlock chain is probably range, one of the others is probably scan modes, Not sure which is which.",
                {
                    k: "t",
                    c: [
                        "\"lightbee\" stuff",
                        {
                            k: "ul",
                            c: [
                                "Is it immaterial?",
                                "Can it be destroyed?",
                                "Can it go through objects?",
                                "What happens if it gets hit by something?",
                                "Can it get trapped somewhere?"
                            ]
                        }
                    ]
                }
            ]
        },
        whatItDoes: {
            k: "ul",
            c: [
                "\"Lightbee\"",
                "Remote Hologram",
                "Teleportation",
                "Truesight",
                "Mage-sight"
            ]
        }
    }),
    orb('CO.A.1', 'Available', 680, -100),
    orb('CO.B.1', 'Available', 750, 'zero'),
    orb('CO.B.2', 'Available'),
    orb('CO.B.3', 'Available'),
    orb('CO.B.4', 'Available', 980, -160),
    orb('CO.C.1', 'Purchased', 840, 20),
    orb('CO.C.2', 'Purchased'),
    orb('CO.C.3', 'Purchased'),
    orb('CO.C.4', 'Purchased'),
    orb('CO.C.5', 'Available', 1300, 140),
    orb('CO.D.1', 'Purchased', 840, 100),
    orb('CO.D.2', 'Available'),
    orb('CO.D.3', 'Available'),
    orb('CO.D.4', 'Available'),
    orb('CO.D.5', 'Available', 1120, 320, null, null, {
        notes: "Possibly a mis-draw, this should probably not be enabled."
    }),
    orb('CO.D.5.VP', 'VanishingPoint', 1340, 300),
    orb('CO.E.1', 'Purchased', 840, 260),
    orb('CO.E.1.A.1', 'Available', 900, 380),
    orb('CO.E.1.B.1', 'Available', 740, 300),
    orb('CO.E.1.B.2', 'Available'),
    orb('CO.E.1.B.3', 'Available'),
    orb('CO.E.1.B.4', 'Available', 700, 580),
    orb('CO.F.1', 'Purchased', 640, 340, null, null, {
        notes: "This is most likely the teleport function since it would probably not have any further unlocks",
        ull: true,
        unlocked: {
            k: "a",
            p: '183'
        }
    })
];
var comOrbEdges = [
    edge('CO', 'CO.A.1', "En", true),
    edge('CO', 'CO.B.1', "En"),
    edge('CO.B.1', 'CO.B.2', 'En'),
    edge('CO.B.2', 'CO.B.3', 'En'),
    edge('CO.B.3', 'CO.B.4', 'En'),
    edge('CO', 'CO.C.1', "En"),
    edge('CO.C.1', 'CO.C.2', 'En'),
    edge('CO.C.2', 'CO.C.3', 'En'),
    edge('CO.C.3', 'CO.C.4', 'En'),
    edge('CO.C.4', 'CO.C.5', 'En'),
    edge('CO', 'CO.D.1', "En"),
    edge('CO.D.1', 'CO.D.2', 'En'),
    edge('CO.D.2', 'CO.D.3', 'En'),
    edge('CO.D.3', 'CO.D.4', 'En'),
    edge('CO.D.4', 'CO.D.5', 'En', true),
    edge('CO.D.5', 'CO.D.5.VP', 'Dis', false, true),
    edge('CO', 'CO.E.1', "En"),
    edge('CO.E.1', 'CO.E.1.A.1', 'En'),
    edge('CO.E.1', 'CO.E.1.B.1', 'En'),
    edge('CO.E.1.B.1', 'CO.E.1.B.2', 'En'),
    edge('CO.E.1.B.2', 'CO.E.1.B.3', 'En'),
    edge('CO.E.1.B.3', 'CO.E.1.B.4', 'En'),
    edge('CO', 'CO.F.1', "En")
];
var lightHookNodes = [
    orb('LH', 'Orb', 520, -420, 'Light Hook Orb', './images/Orbs/LightHookOrb.png', {
        status: "The Hentai Orb",
        notes: "Could this also have a subspace pocket?",
        whatItDoes: {
            k: "ul",
            c: [
                "Tentacle manipulator",
                {
                    c: [
                        "Current max-weight: 15-16 tons. (",
                        {
                            k: "a",
                            p: "404"
                        },
                        ")"
                    ]
                }
            ]
        }
    }),
    orb('LH.A.1', 'Available', 360, -620),
    orb('LH.B.1', 'Purchased', 420, -680),
    orb('LH.B.2', 'Available'),
    orb('LH.B.3', 'Available'),
    orb('LH.B.4', 'Available'),
    orb('LH.B.5', 'Available', 660, -1080),
    orb('LH.C.1', 'Purchased', 540, -620, null, null, {
        notes: {
            k: "ul",
            c: [
                "The max length of the light hook?",
                "The max weight capacity?"
            ]
        }
    }),
    orb('LH.C.2', 'Purchased'),
    orb('LH.C.3', 'Purchased'),
    orb('LH.C.4', 'Available'),
    orb('LH.C.5', 'Available'),
    orb('LH.C.6', 'UnAvailable', 860, -940),
    orb('LH.D.1', 'Purchased', 660, -640),
    orb('LH.D.2', 'Available'),
    orb('LH.D.3', 'Available', 860, -740),
    orb('LH.E.1', 'Purchased', 680, -520),
    orb('LH.E.2', 'Available'),
    orb('LH.E.3', 'Available'),
    orb('LH.E.4', 'Available'),
    orb('LH.E.5', 'Available', 1000, -380)
];
var lightHookEdges = [
    edge('LH', 'LH.A.1', "En", true),
    edge('LH', 'LH.B.1', "En"),
    edge('LH.B.1', 'LH.B.2', "En"),
    edge('LH.B.2', 'LH.B.3', "En"),
    edge('LH.B.3', 'LH.B.4', "En"),
    edge('LH.B.4', 'LH.B.5', "En"),
    edge('LH', 'LH.C.1', "En"),
    edge('LH.C.1', 'LH.C.2', "En"),
    edge('LH.C.2', 'LH.C.3', "En"),
    edge('LH.C.3', 'LH.C.4', "En"),
    edge('LH.C.4', 'LH.C.5', "En"),
    edge('LH.C.5', 'LH.C.6', "En"),
    edge('LH', 'LH.D.1', "En"),
    edge('LH.D.1', 'LH.D.2', "En"),
    edge('LH.D.2', 'LH.D.3', "En"),
    edge('LH.D.3', 'LH.D.4', "En"),
    edge('LH', 'LH.E.1', "En"),
    edge('LH.E.1', 'LH.E.2', "En"),
    edge('LH.E.2', 'LH.E.3', "En"),
    edge('LH.E.3', 'LH.E.4', "En"),
    edge('LH.E.4', 'LH.E.5', "En")
];
var PPONodes = [
    orb('PPO', 'Orb', 'zero', -680, 'PPO / Weapon Orb', './images/Orbs/PPO_Orb.png', {
        status: "PPO; Only if you'r hip.",
        whatItDoes: {
            k: "ul",
            c: [
                "Cutting Beam Weapon",
                "Scatter / Rapid Fire Beam Weapon"
            ]
        }
    }),
    orb('PPO.A.VP', 'VanishingPoint', -340, -640),
    orb('PPO.B.1', 'Available', -280, -740),
    orb('PPO.C.1', 'Purchased', -180, -820, null, null, {
        notes: "This could be max power."
    }),
    orb('PPO.C.2', 'Purchased'),
    orb('PPO.C.3', 'Purchased'),
    orb('PPO.C.4', 'Available'),
    orb('PPO.C.5', 'Available', -480, -1100),
    orb('PPO.D.A', 'Purchased', 'zero', -900, null, null, {
        whatItDoes: "Probabbly the cutting beam.",
        notes: "This section of the tree is probably for the diffrent modes the beam can use."
    }),
    orb('PPO.D.A.1', 'Purchased', null, null, null, null, {
        notes: "Could this allow changeing the <em>power</em> of the beam?"
    }),
    orb('PPO.D.B', 'Available', -140, -940),
    orb('PPO.D.B.1', 'Available'),
    orb('PPO.D.C', 'Available', -140, -1060),
    orb('PPO.D.C.1', 'Available'),
    orb('PPO.D.D', 'Available', 'zero', -1100),
    orb('PPO.D.D.1', 'Available'),
    orb('PPO.D.E', 'Available', 140, -1060),
    orb('PPO.D.E.1', 'Available'),
    orb('PPO.D.F', 'Purchased', 140, -940, 'Rapid Fire', null, {
        whatItDoes: "Unlocks Rapid fire option.",
        notes: "Does this confirm that this unlocks alt-modes for the beam?",
        ull: true,
        unlocked: {
            c: [
                {
                    k: "a",
                    p: '510'
                },
                {
                    k: "sup",
                    c: {
                        c: [
                            "revealed",
                            {
                                k: "a",
                                p: '659'
                            }
                        ]
                    }
                }, 
            ]
        }
    }),
    orb('PPO.D.F.1', 'Available', null, null, null, null, {
        notes: "Could this allow changeing the <em>rate</em> of fire?"
    })
];
var PPOEdges = [
    edge('PPO', 'PPO.A.VP', "Dis", false, true),
    edge('PPO', 'PPO.B.1', "En", true),
    edge('PPO', 'PPO.C.1', "En"),
    edge('PPO.C.1', 'PPO.C.2', "En"),
    edge('PPO.C.2', 'PPO.C.3', "En"),
    edge('PPO.C.3', 'PPO.C.4', "En"),
    edge('PPO.C.4', 'PPO.C.5', "En"),
    edge('PPO', 'PPO.D.A', "En"),
    edge('PPO.D.A', 'PPO.D.A.1', "En"),
    edge('PPO.D.F', 'PPO.D.A', "En"),
    edge('PPO.D.B', 'PPO.D.B.1', "En"),
    edge('PPO.D.A', 'PPO.D.B', "En"),
    edge('PPO.D.C', 'PPO.D.C.1', "En"),
    edge('PPO.D.B', 'PPO.D.C', "En"),
    edge('PPO.D.D', 'PPO.D.D.1', "En"),
    edge('PPO.D.C', 'PPO.D.D', "En"),
    edge('PPO.D.E', 'PPO.D.E.1', "En"),
    edge('PPO.D.D', 'PPO.D.E', "En"),
    edge('PPO.D.F', 'PPO.D.F.1', "En"),
    edge('PPO.D.E', 'PPO.D.F', "En")
];
var FLTNodes = [
    orb('FLT', 'Orb', -520, -420, 'Transportation Orb', './images/Orbs/PropulsionOrb.png', {
        status: "The Flight / Transportation Orb",
        notes: "Why does the Com-Orb have a teleporter if this one provides Propulsion?<br />Does it provide a minor enviromental shield?",
        whatItDoes: {
            k: "ul",
            c: [
                "Flight up to Mach 16.",
                "Removes fear of heights during use",
                "Generates 'Aetherium Causeways' (Wormholes)."
            ]
        }
    }),
    orb('FLT.A.1', 'Available', -700, -240, null, null, {
        notes: "Teleporter unlock looked simlar to this one."
    }),
    orb('FLT.B.1', 'Purchased', -780, -320, 'Flight', null, {
        notes: "This is the speed upgrade tree.",
        whatItDoes: {
            k: "ul",
            c: [
                "Base speed should be 12 Mph or 19 Km/h (5.4 m/s)",
                "This one should provide speed up to 47 Mph or 77 Km/h (21.4 m/s)"
            ]
        }
    }),
    orb('FLT.B.1.A.1', 'Purchased', -900, -320, 'Faster', null, {
        whatItDoes: "Should provide speed up to 191 Mph or 308 Km/h (85.75 m/s)"
    }),
    orb('FLT.B.1.A.2', 'Purchased', null, null, 'Faster', null, {
        whatItDoes: "Should provide speed up to Mach 1. (343 m/s)"
    }),
    orb('FLT.B.1.A.3', 'Purchased', null, null, 'Faster', null, {
        whatItDoes: {
            k: "t",
            c: [
                "Provides speed up to Mach 4. (1372 m/s)",
                {
                    k: "sup",
                    c: {
                        k: "a",
                        p: "670"
                    }
                }
            ]
        }
    }),
    orb('FLT.B.1.A.4', 'Purchased', -1180, -560, 'Faster', null, {
        ull: true,
        unlocked: {
            k: "a",
            p: "660"
        },
        whatItDoes: {
            k: "t",
            c: [
                "Provides speed up to Mach 16. (5488 m/s)",
                {
                    k: "sup",
                    c: {
                        k: "a",
                        p: "670"
                    }
                }
            ]
        }
    }),
    orb('FLT.B.1.B.1', 'Purchased', -1120, -292, `Wormhole`, null, {
        whatItDoes: {
            k: 'ul',
            c: [
                {
                    c: [
                        "Generaates a 'Aetherium Causeway' (Wormhole)",
                        "<br />",
                        {
                            k: 'ul',
                            c: [
                                "Once a causeway closes no known traces are left.",
                                "'Conventional' technology to generate a Aetherium Causeway can only be mounted on capitol ships."
                            ]
                        }
                    ]
                },
                {
                    c: [
                        "Avalible to purchase as of",
                        {
                            k: 'a',
                            p: '660'
                        }
                    ]
                }
            ]
        },
        unlocked: {
            k: "a",
            p: "672"
        },
        notes: "Unknown what the minimum & maximum ranges are if any."
    }),
    orb('FLT.B.1.B.2', 'Available', null, null, null, null, {
        notes: {
            c: [
                "Appears to be unlocked as of",
                {
                    k: 'a',
                    p: '660'
                }
            ]
        }
    }, "off"),
    orb('FLT.B.1.B.3', 'Available', null, null, null, null, {
        notes: {
            c: [
                "Appears to be unlocked as of",
                {
                    k: 'a',
                    p: '660'
                }
            ]
        }
    }, "off"),
    orb('FLT.B.1.B.4', 'Available', null, null, null, null, {
        notes: {
            c: [
                "Appears to be unlocked as of",
                {
                    k: 'a',
                    p: '660'
                }
            ]
        }
    }, "off"),
    orb('FLT.B.1.B.5', 'Available', -1360, -540, null, null, {
        notes: {
            c: [
                "Appears to be unlocked as of",
                {
                    k: 'a',
                    p: '660'
                }
            ]
        }
    }, "off"),
    orb('FLT.B.1.B.5.VP', 'VanishingPoint', -1520, -480),
    orb('FLT.C.1', 'Purchased', -740, -360, null, null, {
        notes: "Turning radius? Inirtial Dampaners? How fast to go from 0 to Mach 6?"
    }),
    orb('FLT.C.2', 'Purchased'),
    orb('FLT.C.3', 'Available'),
    orb('FLT.C.4', 'Available'),
    orb('FLT.C.5', 'Available', -1020, -600),
    orb('FLT.D.1', 'UnAvailable', -760, -520),
    orb('FLT.D.2', 'UnAvailable'),
    orb('FLT.D.3', 'UnAvailable'),
    orb('FLT.D.4', 'UnAvailable'),
    orb('FLT.D.4.VP', 'VanishingPoint', -760, -820),
    orb('FLT.E.VP', 'VanishingPoint', -620, -680)
];
var FLTEdges = [
    edge('FLT', 'FLT.A.1', "En", true),
    edge('FLT', 'FLT.B.1', "En"),
    edge('FLT.B.1', 'FLT.B.1.A.1', "En"),
    edge('FLT.B.1.A.1', 'FLT.B.1.A.2', "En"),
    edge('FLT.B.1.A.2', 'FLT.B.1.A.3', "En"),
    edge('FLT.B.1.A.3', 'FLT.B.1.A.4', "En"),
    edge('FLT.B.1', 'FLT.B.1.B.1', "En"),
    edge('FLT.B.1.A.1', 'FLT.B.1.B.1', "En"),
    edge('FLT.B.1.A.2', 'FLT.B.1.B.1', "En"),
    edge('FLT.B.1.A.3', 'FLT.B.1.B.1', "En"),
    edge('FLT.B.1.A.4', 'FLT.B.1.B.1', "En"),
    edge('FLT.B.1.B.1', 'FLT.B.1.B.2', "En"),
    edge('FLT.B.1.B.2', 'FLT.B.1.B.3', "En"),
    edge('FLT.B.1.B.3', 'FLT.B.1.B.4', "En"),
    edge('FLT.B.1.B.4', 'FLT.B.1.B.5', "En", true),
    edge('FLT.B.1.B.5', 'FLT.B.1.B.5.VP', "Dis", false, true),
    edge('FLT', 'FLT.C.1', "En"),
    edge('FLT.C.1', 'FLT.C.2', "En"),
    edge('FLT.C.2', 'FLT.C.3', "En"),
    edge('FLT.C.3', 'FLT.C.4', "En"),
    edge('FLT.C.4', 'FLT.C.5', "En"),
    edge('FLT', 'FLT.D.1', "Dsl", true),
    edge('FLT.D.1', 'FLT.D.2', "Dis"),
    edge('FLT.D.2', 'FLT.D.3', "Dis"),
    edge('FLT.D.3', 'FLT.D.4', "Dis"),
    edge('FLT.D.4', 'FLT.D.4.VP', "Dis", false, true),
    edge('FLT', 'FLT.E.VP', "Dis", false, true)
];
var MBNodes = [
    orb('MB', 'Orb', -640, 140, 'Shield / Mr Bubble Orb', './images/Orbs/MrBubble_Orb.png', {
        status: "Nothing gets through Mr Bubble!",
        whatItDoes: {
            k: "ul",
            c: [
                "Airtight Shield capible of withstanding most attacks",
                "When Shield is under heavy stress, it can go into a recharge mode, takes moments to return to full."
            ]
        }
    }),
    orb('MB.A.1', 'Available', -680, 400),
    orb('MB.B.1', 'Purchased', -780, 320, null, null, {
        notes: "Max-Size upgrades?"
    }),
    orb('MB.B.2', 'Purchased'),
    orb('MB.B.3', 'Purchased'),
    orb('MB.B.4', 'Purchased'),
    orb('MB.B.5', 'Available', -1240, 414),
    orb('MB.C.1', 'Available', -860, 220),
    orb('MB.C.2', 'Available'),
    orb('MB.C.3', 'UnAvailable', -1000, 296, null, null, {
        notes: "Originally shown as being available for purchase, now shows as locked."
    }, "off"),
    orb('MB.C.3.VP', 'VanishingPoint', -1100, 160),
    orb('MB.D.1', 'Available', -940, 80)
];
var MBEdges = [
    edge('MB', 'MB.A.1', "En", true),
    edge('MB', 'MB.B.1', "En"),
    edge('MB.B.1', 'MB.B.2', "En"),
    edge('MB.B.2', 'MB.B.3', "En"),
    edge('MB.B.3', 'MB.B.4', "En"),
    edge('MB.B.4', 'MB.B.5', "En"),
    edge('MB', 'MB.C.1', "En"),
    edge('MB.C.1', 'MB.C.2', "En"),
    edge('MB.C.2', 'MB.C.3', "Dis", true),
    edge('MB.C.3', 'MB.C.3.VP', "Dis", false, true),
    edge('MB', 'MB.D.1', "En")
];
var UKNodes = [
    orb('UK', 'Orb', -300, 580, 'Still Unknown Orb', './images/Orbs/Unknown_Orb.png', {
        status: "Bringer of alien armadas",
        notes: "Probably some form of computer.",
        whatItDoes: "...What part of unknown do you not understand?"
    }),
    orb('UK.A.1', 'Available', -120, 750, null, null, {
        notes: "Could this unlock a screen or HUD?"
    }),
    orb('UK.B.1', 'Purchased', -280, 800),
    orb('UK.B.2', 'Purchased'),
    orb('UK.B.3', 'Available'),
    orb('UK.B.4', 'Available'),
    orb('UK.B.5', 'Available', -500, 1000),
    orb('UK.C.1', 'Purchased', -560, 560, null, null, {
        notes: "Could this path allow for telepathic control over the orbs? Prehaps this is what lets her move the orbs mentally."
    }),
    orb('UK.C.2', 'Available'),
    orb('UK.C.3', 'Available'),
    orb('UK.C.4', 'Available'),
    orb('UK.C.5', 'Available', -780, 780)
];
var UKEdges = [
    edge('UK', 'UK.A.1', "En", true),
    edge('UK', 'UK.B.1', "En"),
    edge('UK.B.1', 'UK.B.2', "En"),
    edge('UK.B.2', 'UK.B.3', "En"),
    edge('UK.B.3', 'UK.B.4', "En"),
    edge('UK.B.4', 'UK.B.5', "En"),
    edge('UK', 'UK.C.1', "En"),
    edge('UK.C.1', 'UK.C.2', "En"),
    edge('UK.C.2', 'UK.C.3', "En"),
    edge('UK.C.3', 'UK.C.4', "En"),
    edge('UK.C.4', 'UK.C.5', "En")
];
var LSNodes = [
    orb('LS', 'Orb', 300, 580, 'Life Support Orb', './images/Orbs/LifeSupport_Orb.png', {
        status: "Pine Fresh",
        notes: "Can it be used in a vacuum?",
        whatItDoes: {
            k: "ul",
            c: [
                "Created air underwater, can provide breathable air from water and air(that has insufficent O2 content)."
            ]
        }
    }),
    orb('LS.A.1', 'Purchased', 520, 540, null, null, {
        notes: "Could this allow it to create air in a vacuum?"
    }),
    orb('LS.B.1', 'Purchased', 460, 660, null, null, {
        notes: "Sents? Possibly range of effect."
    }),
    orb('LS.B.2', 'Available'),
    orb('LS.B.3', 'Available'),
    orb('LS.C.1', 'Available', 200, 760),
    orb('LS.C.2', 'Available'),
    orb('LS.C.3', 'Available'),
    orb('LS.C.4', 'Available', 500, 860)
];
var LSEdges = [
    edge('LS', 'LS.A.1', "En", true),
    edge('LS', 'LS.B.1', "En"),
    edge('LS.B.1', 'LS.B.2', "En"),
    edge('LS.B.2', 'LS.B.3', "En"),
    edge('LS', 'LS.C.1', "En"),
    edge('LS.C.1', 'LS.C.2', "En"),
    edge('LS.C.2', 'LS.C.3', "En"),
    edge('LS.C.3', 'LS.C.4', "En")
];
var __outerCenterEdges = [];
function createEdgeSet(pair) {
    var aN = pair.split("-")[0];
    var bN = pair.split("-")[1];
    var aE = edge(aN, pair, "En", true, false, true, "auto");
    var bE = edge(bN, pair, "En", true, false, true, "auto");
    __outerCenterEdges.push(aE);
    __outerCenterEdges.push(bE);
}
var __outerCenterNodes = [
    orb('LS-UK', 'Off'),
    orb('LS-MB', 'Off'),
    orb('LS-FLT', 'Off'),
    orb('LS-PPO', 'Off'),
    orb('LS-LH', 'Off'),
    orb('LS-CO', 'Off'),
    orb('UK-MB', 'Off'),
    orb('UK-FLT', 'Off'),
    orb('UK-PPO', 'Off'),
    orb('UK-LH', 'Off'),
    orb('UK-CO', 'Off'),
    orb('MB-FLT', 'Off'),
    orb('MB-PPO', 'Off'),
    orb('MB-LH', 'Off', null, null, null, null, {
        notes: "Could this enable use of the lighthook within the bubble?"
    }),
    orb('MB-CO', 'Off'),
    orb('FLT-PPO', 'On', null, null, null, null, {
        notes: "Some sort of synergy? Why only between these two orbs? Could decrease kickback from the PPO orb."
    }),
    orb('FLT-LH', 'Off'),
    orb('FLT-CO', 'Off'),
    orb('PPO-LH', 'Off'),
    orb('PPO-CO', 'Off'),
    orb('LH-CO', 'Off')
];
for(var index in __outerCenterNodes){
    createEdgeSet(__outerCenterNodes[index].id);
}
var outerCenterNodes = __outerCenterNodes;
var outerCenterEdges = __outerCenterEdges;
var innerCenterNodes = [
    orb('Center.True', 'TrueCenter', 'zero', 'zero', null, null, {
        notes: {
            k: "ul",
            c: [
                "most likely will become fully enabled if all connected nodes were enabled.",
                "May enable viewing past the Vanishing Point upgrades.",
                "Possibly unlocks a new orb",
                "Due to the blasted tree automatically rotaiting, I don't know which one was bought yet, Image will be updated as soon as it is clarrified."
            ]
        },
        unlocked: {
            k: 'ul',
            c: [
                {
                    c: [
                        "Wedge 1",
                        "Prior to optaining orbs"
                    ]
                },
                {
                    c: [
                        "Wedge 2",
                        "Prior to optaining orbs"
                    ]
                },
                {
                    c: [
                        "Wedge 3",
                        {
                            k: 'a',
                            p: '672'
                        }
                    ]
                }
            ]
        },
        name: "True Center"
    }),
    orb('Center.CO', 'Off', 210, 45, null, null, {
        notes: "Most likely connected in some way to the Communications Orb"
    }, "off"),
    orb('Center.LH', 'Off', 165, -150, null, null, {
        notes: "Most likely connected in some way to the Light Hook Orb"
    }, "off"),
    orb('Center.PPO', 'Off', 'zero', -240, null, null, {
        notes: {
            k: "ul",
            c: [
                "Most likely connected in some way to the Weapons Orb",
                {
                    c: [
                        "Unclear if this or Center.UK was purchaced. (",
                        {
                            k: "a",
                            p: "672"
                        },
                        ")"
                    ]
                }
            ]
        }
    }, "off"),
    orb('Center.FLT', 'On', -165, -150, null, null, {
        notes: "Most likely connected in some way to the Propulsion Orb"
    }, "off"),
    orb('Center.MB', 'On', -210, 45, null, null, {
        notes: "Most likely connected in some way to the Shield Orb"
    }, "off"),
    orb('Center.UK', 'Off', -105, 180, null, null, {
        notes: {
            k: "ul",
            c: [
                "Most likely connected in some way to the 'Still Unknown' Orb",
                {
                    c: [
                        "Unclear if this or Center.PPO was purchaced. (",
                        {
                            k: "a",
                            p: "672"
                        },
                        ")"
                    ]
                }
            ]
        }
    }, "off"),
    orb('Center.LS', 'Off', 105, 180, null, null, {
        notes: "Most likely connected in some way to the Life Support Orb"
    }, "off")
];
var innerCenterHidden = [
    edge('LH', 'Center.LH', 'Invs', false, false, true, 'auto'),
    edge('CO', 'Center.CO', 'Invs', false, false, true, 'auto'),
    edge('PPO', 'Center.PPO', 'Invs', false, false, true, 'auto'),
    edge('FLT', 'Center.FLT', 'Invs', false, false, true, 'auto'),
    edge('MB', 'Center.MB', 'Invs', false, false, true, 'auto'),
    edge('UK', 'Center.UK', 'Invs', false, false, true, 'auto'),
    edge('LS', 'Center.LS', 'Invs', false, false, true, 'auto')
];
var innerCenterVisible = [
    edge('Center.LH', 'Center.True', 'En', false, false, true, 'auto'),
    edge('Center.CO', 'Center.True', 'En', false, false, true, 'auto'),
    edge('Center.PPO', 'Center.True', 'En', false, false, true, 'auto'),
    edge('Center.FLT', 'Center.True', 'En', false, false, true, 'auto'),
    edge('Center.MB', 'Center.True', 'En', false, false, true, 'auto'),
    edge('Center.UK', 'Center.True', 'En', false, false, true, 'auto'),
    edge('Center.LS', 'Center.True', 'En', false, false, true, 'auto'),
    edge('Center.LH', 'Center.CO', 'Dis', true, false, false, 'auto'),
    edge('Center.PPO', 'Center.LH', 'Dis', true, false, false, 'auto'),
    edge('Center.FLT', 'Center.PPO', 'Dis', true, false, false, 'auto'),
    edge('Center.MB', 'Center.FLT', 'Dis', true, false, false, 'auto'),
    edge('Center.UK', 'Center.MB', 'Dis', true, false, false, 'auto'),
    edge('Center.LS', 'Center.UK', 'Dis', true, false, false, 'auto'),
    edge('Center.CO', 'Center.LS', 'Dis', true, false, false, 'auto')
];
var innerCenterEdges = [
    ...innerCenterHidden,
    ...innerCenterVisible
];
var absoluteDefaultMetadata = {
    namePatern: "|id|",
    status: "Data Missing",
    unlocked: {
        text: "Data Missing"
    },
    notes: "No current thoughts.",
    whatItDoes: "Unknown."
};
var allGroupDefaultMetadata = {
    "Purchased": {
        group: "Purchased",
        status: "Purchased",
        unlocked: "Unknown / Prior to Sydney Obtaining the Orbs.",
        color: {
            background: '#007F0E',
            border: '#073B0B'
        }
    },
    "Available": {
        group: "Available",
        status: "Unlocked, Awaiting Purchase.",
        unlocked: "It hasn't been Purchased yet!",
        color: {
            background: '#FE0000',
            border: '#800000'
        }
    },
    "UnAvailable": {
        group: "UnAvailable",
        status: "Locked",
        unlocked: "It can't be Purchased yet!",
        color: {
            background: '#FF00DC',
            border: '#4E0042'
        }
    },
    "On": {
        group: "On",
        status: "On",
        unlocked: "Unknown / Prior to Sydney Obtaining the Orbs.",
        color: {
            background: '#007F0E',
            border: '#073B0B'
        }
    },
    "Off": {
        group: "Off",
        status: "Locked",
        unlocked: "It has not been turned on!",
        color: {
            background: '#FE0000',
            border: '#800000'
        }
    },
    "Disabled": {
        group: "Disabled",
        status: "Disabled",
        unlocked: "It can't be turned on yet!",
        color: {
            background: '#FF00DC',
            border: '#4E0042'
        }
    },
    "VanishingPoint": {
        group: "VanishingPoint",
        status: "Presumed to exist however it's not shown",
        unlocked: "It's metadata!",
        color: {
            background: 'gray',
            border: 'darkgray'
        }
    },
    "Hidden": {
        group: "Hidden",
        status: "Used to display",
        unlocked: "It's metadata!",
        color: {
            background: 'white',
            border: 'white'
        }
    },
    "Orb": {
        group: "Orb",
        namePatern: "|label| (|id|)",
        status: "It's a orb!",
        unlocked: "It's a orb!",
        color: {
            background: 'white',
            border: 'white'
        }
    },
    "TrueCenter": {
        group: "TrueCenter",
        namePatern: "|name| (|id|)",
        status: "It's a weird one that requires multiple other unlocks...",
        unlocked: "2/7 were unlocked prior to prior to Sydney obtaining the orbs",
        color: {
            background: 'linear-gradient(to right, #007f0e 42%,#fe0000 43%)',
            border: 'white'
        }
    }
};
var generalSettings = {
    symbols: {
        flag: {
            unlockWithLink: "📆",
            unlockWithoutLink: "📅",
            whatItDoes: "🔧",
            hasNotes: "💬"
        }
    },
    noNode: {
        name: "No Node Clicked",
        color: "white",
        flagDots: "",
        status: "Select a node to view.",
        unlocked: "Select a node to view.",
        whatItDoes: "Select a node to view.",
        note: "Select a node to view.",
        param: undefined
    }
};
var orbNodes = [
    ...comOrbNodes,
    ...lightHookNodes,
    ...PPONodes,
    ...FLTNodes,
    ...MBNodes,
    ...UKNodes,
    ...LSNodes
];
var orbEdges = [
    ...comOrbEdges,
    ...lightHookEdges,
    ...PPOEdges,
    ...FLTEdges,
    ...MBEdges,
    ...UKEdges,
    ...LSEdges
];
var allNodes = [
    ...orbNodes,
    ...outerCenterNodes,
    ...innerCenterNodes
];
var allEdges = [
    ...orbEdges,
    ...outerCenterEdges,
    ...innerCenterEdges
];
var orbDataset = {
    nodes: allNodes,
    edges: allEdges,
    nodeStyles: nodeGroupStyles
};
var options = {
    'defaultMD': absoluteDefaultMetadata,
    'groupMD': allGroupDefaultMetadata,
    'settings': generalSettings
};
function makeMap() {
    var nodes = new vis.DataSet(orbDataset.nodes);
    var edges = new vis.DataSet(orbDataset.edges);
    var container = document.getElementById('dataVisualization');
    var data2 = {
        nodes: nodes,
        edges: edges
    };
    var options1 = {
        groups: orbDataset.nodeStyles,
        interaction: {
            navigationButtons: true,
            keyboard: true
        },
        layout: {
            improvedLayout: false,
            randomSeed: 3
        }
    };
    var network = new vis.Network(container, data2, options1);
    return network;
}
var aggregator = (baseClass, ...mixins)=>{
    let base = class _Combined extends baseClass {
        constructor(...args){
            super(...args);
            mixins.forEach((mixin)=>{
                mixin.prototype.initializer.call(this);
            });
        }
    };
    let copyProps = (target, source)=>{
        Object.getOwnPropertyNames(source).concat(Object.getOwnPropertySymbols(source)).forEach((prop)=>{
            if (prop.match(/^(?:constructor|prototype|arguments|caller|name|bind|call|apply|toString|length)$/)) return;
            Object.defineProperty(target, prop, Object.getOwnPropertyDescriptor(source, prop));
        });
    };
    mixins.forEach((mixin)=>{
        copyProps(base.prototype, mixin.prototype);
        copyProps(base, mixin);
    });
    return base;
};
class toolkit {
    constructor(cssSearch1, baseElement, errorOnEmpty){
        this.extentions = [
            "baseToolkit"
        ];
        this.ele = false;
        this.elements = [];
        this.errors = {
            NO_ELEMENTS: "No Elements Selected"
        };
        errorOnEmpty = errorOnEmpty || false;
        this.errorOnEmpty = errorOnEmpty;
        if (cssSearch1 !== null) {
            baseElement = baseElement || document;
            this.elements = baseElement.querySelectorAll(cssSearch1);
            if (this.elements.length > 0) {
                this.ele = true;
            } else {
                this.ele = false;
            }
        }
    }
    addExtention(newExtention) {
        this.extentions.push(newExtention);
    }
    getExtentions() {
        return this.extentions;
    }
    hasElement() {
        if (!this.ele && this.errorOnEmpty) {
            throw this.errors.NO_ELEMENTS;
        }
    }
    doEach(f) {
        this.hasElement();
        this.elements.forEach(f);
    }
    parrents() {
        this.hasElement();
        let newElements = [];
        this.doEach(function(oldElement, index1) {
            newElements.push(oldElement.parentElement);
        });
        this.elements = newElements;
        if (this.elements.length > 0) {
            this.ele = true;
        } else {
            this.ele = false;
        }
    }
    searchChildren(cssSearch) {
        this.hasElement();
        if (cssSearch !== null) {
            let newElements = [];
            this.doEach(function(oldElement, index1) {
                newElements = newElements.concat(Array.prototype.slice.call(oldElement.querySelectorAll(cssSearch)));
            });
            this.elements = newElements;
            if (this.elements.length > 0) {
                this.ele = true;
            } else {
                this.ele = false;
            }
        } else {
            this.ele = false;
            this.elements = [];
        }
    }
}
class contentToolkitExtention {
    initializer() {
        this.addExtention("contentToolkitExtention");
    }
    replaceText(text) {
        this.hasElement();
        this.doEach(function(element, index1) {
            element.textContent = text;
        });
    }
    replaceHTML(html) {
        this.hasElement();
        this.doEach(function(element, index1) {
            element.innerHTML = html;
        });
    }
}
class styleToolkitExtention {
    initializer() {
        this.addExtention("styleToolkitExtention");
        this.tri = {
            ON: 1,
            OFF: 0,
            TOGGLE: 0.5
        };
    }
    changeStyle(property, value) {
        this.hasElement();
        value = value || "";
        this.doEach(function(element, index1) {
            element.style[property] = value;
        });
    }
    modifyClass(className, fixed) {
        this.hasElement();
        fixed = fixed || this.tri.TOGGLE;
        let f = function(element, index1) {
            throw "This should never happen.";
        };
        switch(fixed){
            case this.tri.ON:
                f = function(element, index1) {
                    element.classList.add(className);
                };
                break;
            case this.tri.OFF:
                f = function(element, index1) {
                    element.classList.remove(className);
                };
                break;
            case this.tri.TOGGLE:
                f = function(element, index1) {
                    element.classList.toggle(className);
                };
        }
        this.doEach(f);
    }
}
class urlToolkitExtention {
    initializer() {
        this.addExtention("urlToolkitExtention");
    }
    getUrlParam(param) {
        return this.getUrlParams()[param];
    }
    getUrlParams() {
        var url_string = window.location.href;
        var url = new URL(url_string);
        var params = {
        };
        for (var pair of url.searchParams.entries()){
            params[pair[0]] = pair[1];
        }
        this.params = params;
        return params;
    }
    setParams(params) {
        var fileName = window.location.href.split("/").slice(-1) + "";
        fileName = fileName.split('?')[0];
        this.updatePageURL(fileName + "?" + params);
    }
    updatePageURL(url) {
        window.history.replaceState(null, null, url);
    }
}
function jsonTextProsessor(options1) {
    return new JsonTextProsessor(options1);
}
class JsonTextProsessor {
    constructor(options1){
        var defaultOptions = {
            linkStart: ""
        };
        this.options = Object.assign({
        }, defaultOptions, options1);
    }
    prosessText(data) {
        return this.__isString(data);
    }
    __textProsessor(data) {
        if (data.k) {
            switch(data.k){
                case "t":
                    return this.__str(data);
                    break;
                case "ul":
                    return this.__uList(data);
                    break;
                case "a":
                    return this.__link(data);
                    break;
                case "sup":
                    return this.__superScript(data);
                    break;
            }
        } else {
            return this.__str(data);
        }
    }
    __isString(content) {
        if (typeof content === 'string') {
            return content;
        } else {
            return this.__textProsessor(content);
        }
    }
    __str(data) {
        var tempText = "";
        for(var x1 in data.c){
            tempText += " " + this.__isString(data.c[x1]);
        }
        return tempText;
    }
    __uList(data) {
        var tempText = "<ul>";
        for(var x2 in data.c){
            tempText += "<li>" + this.__isString(data.c[x2]) + "</li>";
        }
        tempText += "</ul>";
        return tempText;
    }
    __link(data) {
        if (data.p && this.options.linkModderFunction) {
            return this.options.linkModderFunction(data.p);
        } else {
            var tempLink = this.options.linkStart + data.l;
            return '<a href="' + tempLink + '">' + this.__isString(data.t) + '</a>';
        }
    }
    __superScript(data) {
        return "<sup>" + this.__isString(data.c) + "</sup>";
    }
}
class tk extends aggregator(toolkit, contentToolkitExtention, styleToolkitExtention, urlToolkitExtention) {
}
const LOADING = "Loading";
const NONE = "None";
var comicPointers = LOADING;
var lastNodeList = NONE;
{
    var xmlhttp = new XMLHttpRequest();
    var finalQ = "./data/autoComicSet.json";
    xmlhttp.open('GET', finalQ, true);
    xmlhttp.onreadystatechange = function() {
        if (xmlhttp.readyState == 4) {
            if (xmlhttp.status == 200) {
                comicPointers = JSON.parse(xmlhttp.responseText);
                console.log("pointers downloaded");
                if (lastNodeList !== NONE) {
                    mapClick(lastNodeList);
                }
            }
        }
    };
    xmlhttp.send(null);
}function modLinks(comicNumber) {
    let pointer = comicPointers[comicNumber];
    let text = "";
    if (comicPointers == LOADING) {
        let reloadText = 'ev = new Event("submit"); document.getElementById("searchForm").dispatchEvent(ev);';
        text = "<a onclick='" + reloadText + "' title='Click to load.'>Click to fix</a>";
    } else {
        if (pointer) {
            pointer.url = "https://grrlpowercomic.com/archives/comic/" + pointer.id;
            pointer.page = "#" + pointer.number;
            pointer.hover = pointer.page + " - " + pointer.title;
            text = "<a href='" + pointer.url + "' title='" + pointer.hover + "'>" + pointer.page + "</a>";
        } else {
            text = "<a title='POINTER NOT FOUND'>#" + comicNumber + "</a>";
        }
    }
    return text;
}
var jtp = jsonTextProsessor({
    linkStart: "http://grrlpowercomic.com/archives/",
    linkModderFunction: modLinks
});
function findNode(searchID) {
    return orbDataset.nodes.find(function(node) {
        return node.id == searchID;
    });
}
function flagDecorations(data2) {
    var flags = " ";
    if (data2.nodeUnlockedBasic) {
        if (data2.nodeUnlockedLink) {
            flags += "<span title='Unlock Time W Link'>" + options.settings.symbols.flag.unlockWithLink + "</span> ";
        } else {
            flags += "<span title='Unlock Time W/O Link'>" + options.settings.symbols.flag.unlockWithoutLink + "</span> ";
        }
    }
    if (data2.nodeWhatItDoes) {
        flags += "<span title='Known Function'>" + options.settings.symbols.flag.whatItDoes + "</span> ";
    }
    if (data2.nodeNote) {
        flags += "<span title='Has Theories'>" + options.settings.symbols.flag.hasNotes + "</span> ";
    }
    flags = flags.slice(0, -1);
    return flags;
}
function runTextProsessor(data2) {
    if (data2 !== undefined) {
        return jtp.prosessText(data2);
    } else {
        return null;
    }
}
function setWindowText(name, color, flag, status1, unlock, whatItDoes, note, param) {
    new tk("#NID").replaceHTML(name);
    new tk("#NFlag").changeStyle("background", color);
    new tk("#NFlag").replaceHTML(flag);
    new tk("#NStatus").replaceHTML(status1);
    new tk("#NObtain").replaceHTML(unlock);
    new tk("#NFunction").replaceHTML(whatItDoes);
    new tk("#NHypothesis").replaceHTML(note);
    if (param) {
        new tk().setParams('node=' + param);
    } else {
        new tk().setParams('');
    }
}
function nameGenerator(clickedNode, metadata) {
    let tempText = metadata.namePatern;
    let tempID = clickedNode.id;
    if (metadata.beautifulID) {
        tempID = clickedNode.data.beautifulID;
    }
    tempText = tempText.replace("|id|", tempID);
    tempText = tempText.replace("|label|", clickedNode.label);
    return tempText.replace("|name|", metadata.name);
}
function mapClick(properties) {
    lastNodeList = properties;
    var ids = properties.nodes;
    if (ids.length >= 1) {
        if (ids.length == 1) {
            let searchID = ids[0];
            var clickedNode = findNode(searchID);
        }
        {
            let groupDefaults = {
            };
            if (options.groupMD[clickedNode.group] !== undefined) {
                groupDefaults = options.groupMD[clickedNode.group];
            }
            let defaultMD = Object.assign({
            }, options.defaultMD, groupDefaults);
            let nodeMD = {
            };
            if (clickedNode.data !== undefined) {
                nodeMD = clickedNode.data;
            }
            var finalMD = Object.assign({
            }, defaultMD, nodeMD);
        }
        var flagDots = flagDecorations(finalMD);
        var finalName = nameGenerator(clickedNode, finalMD);
        finalMD.unlocked = runTextProsessor(finalMD.unlocked);
        finalMD.whatItDoes = runTextProsessor(finalMD.whatItDoes);
        finalMD.notes = runTextProsessor(finalMD.notes);
        setWindowText(finalName, finalMD.color.background, flagDots, finalMD.status, finalMD.unlocked, finalMD.whatItDoes, finalMD.notes, clickedNode.id);
    } else {
        let nn = options.settings.noNode;
        setWindowText(nn.name, nn.color, nn.flagDots, nn.status, nn.unlocked, nn.whatItDoes, nn.note, nn.param);
    }
}
class eventToolkitExtention {
    initializer() {
        this.addExtention("eventToolkitExtention");
        this.listening = {
        };
    }
    addListener(eventType, funcToDo) {
        this.hasElement();
        this.doEach(function(element, index1) {
            element.addEventListener(eventType, funcToDo);
        });
        if (this.listening[eventType] == undefined) {
            this.listening[eventType] = [];
        }
        this.listening[eventType].push(funcToDo);
    }
    removeListener(eventType, funcToNotDo) {
        this.hasElement();
        this.doEach(function(element, index1) {
            element.removeEventListener(eventType, funcToNotDo);
        });
        if (this.listening[eventType]) {
            let index1 = this.listening[eventType].indexOf(funcToNotDo);
            if (index1 >= 0) {
                this.listening[eventType].splice(index1, 1);
            }
        }
    }
    removeAllListeners(eventType) {
        this.hasElement();
        let tkSelf = this;
        this.listening[eventType].forEach(function(listenerToRemove, index1) {
            tkSelf.doEach(function(element, index2) {
                element.removeEventListener(eventType, listenerToRemove);
            });
        });
        this.listening[eventType] = [];
    }
}
const NOTHING = "Nothing";
class formToolkitExtention {
    initializer() {
        this.addExtention("formToolkitExtention");
    }
    value(value) {
        this.hasElement();
        value = value || NOTHING;
        if (value === NOTHING) {
            var values = [];
            this.doEach(function(element, index1) {
                values.push(element.value);
            });
            if (values.length == 1) {
                return values[0];
            } else {
                return values;
            }
        } else {
            this.doEach(function(element, index1) {
                element.value = value;
            });
        }
    }
    setValididy(message) {
        this.hasElement();
        this.doEach(function(element, index1) {
            element.setCustomValidity(message);
        });
    }
    tellInvalid(message) {
        this.hasElement();
        this.doEach(function(element, index1) {
            element.reportValidity();
        });
    }
    toggleEnabled(status) {
        this.hasElement();
        status = status || "toggle";
        switch(status.toLowerCase()){
            case "toggle":
                this.elements.forEach(function(element, index1) {
                    element.disabled = s;
                });
                break;
            case "on":
                this.elements.forEach(function(element, index1) {
                    element.disabled = false;
                });
                break;
            case "off":
                this.elements.forEach(function(element, index1) {
                    element.disabled = s;
                });
                break;
        }
    }
}
class tk1 extends aggregator(toolkit, eventToolkitExtention, formToolkitExtention, urlToolkitExtention) {
}
var network = null;
function addSearch(passedNetwork) {
    network = passedNetwork;
    __checkURL();
    new tk1('#searchForm').addListener("submit", __onSearch);
    new tk1('#NIDS').addListener("change", __onTextChange);
}
function __checkURL() {
    var nodeToFind = new tk1().getUrlParam('node');
    if (nodeToFind !== undefined) {
        nodeToFind = nodeToFind.toUpperCase();
        if (findNode(nodeToFind) !== undefined) {
            try {
                network.selectNodes([
                    nodeToFind
                ], false);
                new tk1('#NIDS').value(nodeToFind);
                mapClick({
                    nodes: [
                        nodeToFind
                    ]
                });
            } catch (e) {
            }
        } else {
            if (nodeToFind !== "") {
                new tk1('#NIDS').setValididy("Node Not Found.");
                new tk1('#NIDS').tellInvalid();
            }
        }
    }
}
function __onSearch(event) {
    event.preventDefault();
    var nodeToFind = new tk1('#NIDS').value().toUpperCase();
    if (nodeToFind !== undefined) {
        nodeToFind = nodeToFind.toUpperCase();
        if (findNode(nodeToFind) !== undefined) {
            try {
                network.selectNodes([
                    nodeToFind
                ], false);
                mapClick({
                    nodes: [
                        nodeToFind
                    ]
                });
                new tk1('#NIDS').value(nodeToFind);
            } catch (e) {
                new tk1('#NIDS').setValididy("Node Not Found.");
                new tk1('#NIDS').tellInvalid();
            }
        } else {
            if (nodeToFind !== "") {
                new tk1('#NIDS').setValididy("Node Not Found.");
                new tk1('#NIDS').tellInvalid();
            }
        }
    }
}
function __onTextChange(event) {
    new tk1('#NIDS').setValididy("");
}
var network1 = null;
function onLoadScript() {
    network1 = makeMap();
    network1.on('click', function(properties) {
        mapClick(properties);
    });
    addSearch(network1);
}
window.onload = function() {
    onLoadScript();
};
