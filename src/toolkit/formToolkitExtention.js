/* esversion: 6 */

// Adds form tools to the toolkit

/**
 * A constant for if nothing
 * @type {String}
 * @todo finish the documentation for this one.
 */
export const NOTHING = "Nothing";

/**
 * Extention for working with forms.
 * @extends {toolkit}
 */
export class formToolkitExtention {

  /**
   * Prevents the agragator from giveing fits.
   * @public
   * @ignore
   */
  initializer(){
    this.addExtention("formToolkitExtention");
  }

  value(value){
    this.hasElement();
    value = value || NOTHING;
    if (value === NOTHING) {
      var values = [];
      this.doEach(function (element, index) {
        values.push(element.value);
      });
      if (values.length == 1) {
        return values[0];
      } else {
        return values;
      }
    } else {
      this.doEach(function (element, index) {
        element.value = value;
      });
    }
  }

  setValididy(message) {
    this.hasElement();
    this.doEach(function (element, index) {
      element.setCustomValidity(message);
    });
  }

  tellInvalid(message) {
    this.hasElement();
    this.doEach(function (element, index) {
      element.reportValidity();
    });
  }

  toggleEnabled(status) {
    this.hasElement();
    status = status || "toggle";
    switch (status.toLowerCase()) {
      case "toggle":
        this.elements.forEach( function (element, index) {
          element.disabled = s;
        });
        break;
      case "on":
        this.elements.forEach( function (element, index) {
          element.disabled = false;
        });
        break;
      case "off":
        this.elements.forEach( function (element, index) {
          element.disabled = s;
        });
        break;
    }
  }

};
