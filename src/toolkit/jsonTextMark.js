/* jshint esversion:6 */

/* "I'll bould my own jQuery! Without bloat, and ES6 Compatable!" */

export function jsonTextProsessor(options) {
  return (new JsonTextProsessor(options));
}

class JsonTextProsessor {
  constructor(options) {
    var defaultOptions = {linkStart: ""};
    this.options = Object.assign({}, defaultOptions, options);
  }

  prosessText(data) {
    return this.__isString(data);
  }

  __textProsessor(data) {
    if (data.k) {
      switch(data.k){
        case "t":
          return this.__str(data);
          break;
        case "ul":
          return this.__uList(data);
          break;
        case "a":
          return this.__link(data);
          break;
        case "sup":
          return this.__superScript(data);
          break;
      }
    } else {
      return this.__str(data);
    }
  }

  __isString(content){
    if(typeof content === 'string'){
      return content;
    } else {
      return this.__textProsessor(content);
    }
  }

  __str(data) {
    var tempText = "";
    for (var x in data.c) {
      tempText += " " + this.__isString(data.c[x]);
    }
    return tempText;
  }

  __uList(data) {
    var tempText = "<ul>";
    for (var x in data.c){
      tempText += "<li>" + this.__isString(data.c[x]) + "</li>";
    }
    tempText += "</ul>";
    return tempText;
  }

  __link(data) {
    // if there is a pointer and the modder function is active, use it.
    if(data.p && this.options.linkModderFunction){
      return this.options.linkModderFunction(data.p);
    } else {
      var tempLink = this.options.linkStart + data.l;
      return ('<a href="' + tempLink + '">' + this.__isString(data.t) + '</a>');
    }
  }

  __superScript(data) {
    return "<sup>" + this.__isString(data.c) + "</sup>";
  }
}
