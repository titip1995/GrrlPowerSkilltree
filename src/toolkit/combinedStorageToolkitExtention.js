/* esversion: 6 */


/**
 * Base extention for saveing data.
 * @extends {toolkit}
 */
class baseStorageToolkitExtention {
    /**
     * Sets up the members for this extention.
     * Should really be 'initializer' instead of this mess, but it doesn't work like that.
     * @public
     */
    baseStorageToolkitExtentionInitializer(){
      this.addExtention("baseStorageToolkitExtention");
      /**
       * If the browser supports sotrage.
       * @public
       */
      this.has_storage = false;
      if (typeof(Storage) !== "undefined") {
        this.has_sotrage = true;
      } else {
        this.has_sotrage = false;
      }
      /**
       * The namespace that the data should be saved in.
       * This helps with ensureing that data is somewhat protected.
       * @private
       */
      this.__namespace = "";
    }

    /**
     * Set's namespace.
     * @public
     * @param {String} [NameSpace=""] - The namespace all future data should be saved / loaded from.
     */
    set namespace(NameSpace){
      NameSpace = NameSpace || "";
      this.__namespace = NameSpace;
    }

    /**
     * Get's current namespace.
     * @public
     * @returns {String} - Returns current namespace as a string, if no namespace is set it will return a empty string.
     */
    get namespace(){
      return this.__namespace;
    }
}


/**
 * Extention for saveing data across sessons.
 * @extends {baseStorageToolkitExtention}
 */
export class permanentStorageToolkitExtention extends baseStorageToolkitExtention {

  /**
   * Prevents the agragator from giveing fits.
   * initializer comes from {baseStorageToolkitExtention}
   * Really janky since super() was giveing ESDoc fits.
   * @public
   * @ignore
   */
  initializer(){
    this.baseStorageToolkitExtentionInitializer();
    this.addExtention("permanentStorageToolkitExtention");
  }

  /**
   * Saves data to the permanant storage
   * @public
   * @param {String} prop - property to set.
   * @param {String} value - value to set the property to.
   */
  saveDatPerm(prop, value){
    localStorage.setItem(this.__namespace + prop, value);
  }

  /**
   * Retrevies data from the permanant storage
   * @public
   * @param {String} prop - property to get.
   * @returns {String} value the property is currently set to.
   */
  loadDatPerm(prop){
    return localStorage.getItem(this.__namespace + prop);
  }

  /**
   * Deletes data from the permanant storage
   * @public
   * @param {String} prop - property to set.
   */
  dropDatPerm(prop){
    localStorage.removeItem(this.__namespace + prop);
  }

};

/**
 * Extention for saveing data for a single session.
 * @extends {baseStorageToolkitExtention}
 */
export class sessionStorageToolkitExtention extends baseStorageToolkitExtention {

  /**
   * Prevents the agragator from giveing fits.
   * initializer comes from {baseStorageToolkitExtention}
   * Really janky since super() was giveing ESDoc fits.
   * @public
   * @ignore
   */
  initializer(){
    this.baseStorageToolkitExtentionInitializer();
    this.addExtention("sessionStorageToolkitExtention");
  }

  /**
   * Saves data to session storage
   * @public
   * @param {String} prop - property to set.
   * @param {String} value - value to set the property to.
   */
  saveDatSession(prop, value){
    sessionStorage.setItem(this.namespace + prop, value);
  }

  /**
   * Retrevies data from the session storage
   * @public
   * @param {String} prop - property to get.
   * @returns {String} value the property is currently set to.
   */
  loadDatSession(prop){
    sessionStorage.getItem(this.namespace + prop);
  }

  /**
   * Deletes data from the session storage
   * @public
   * @param {String} prop - property to set.
   */
  dropDatSession(prop){
    sessionStorage.removeItem(this.namespace + prop);
  }

};
