/* jshint esversion:6 */
import {orb} from "./mapClasses/OrbClass.js";
// orb(ID, GROUP, X, Y, LABEL, IMAGE, DATA)
// any can be null except ID and GROUP.
import {edge} from "./mapClasses/EdgeClass.js";
// edge(from, to, status, dashed, arrow, straight, length)



// Light Hook Orb
export var lightHookNodes = [
    orb( 'LH', 'Orb', 520, -420, 'Light Hook Orb', './images/Orbs/LightHookOrb.png', {
      status:"The Hentai Orb",
      notes:"Could this also have a subspace pocket?",
      whatItDoes:{
        k:"ul",
        c:[
          "Tentacle manipulator",
          {c: ["Current max-weight: 15-16 tons. (", {k:"a",p:"404"}, ")"]}
        ]
      }
    }),
    orb( 'LH.A.1', 'Available', 360, -620),
    orb( 'LH.B.1', 'Purchased', 420, -680),
    orb( 'LH.B.2', 'Available'),
    orb( 'LH.B.3', 'Available'),
    orb( 'LH.B.4', 'Available'),
    orb( 'LH.B.5', 'Available', 660, -1080),
    orb( 'LH.C.1', 'Purchased', 540, -620, null, null, {
      notes:{
        k:"ul",
        c:[
          "The max length of the light hook?",
          "The max weight capacity?"
        ]
      }
    }),
    orb( 'LH.C.2', 'Purchased'),
    orb( 'LH.C.3', 'Purchased'),
    orb( 'LH.C.4', 'Available'),
    orb( 'LH.C.5', 'Available'),
    orb( 'LH.C.6', 'UnAvailable', 860, -940),
    orb( 'LH.D.1', 'Purchased', 660, -640),
    orb( 'LH.D.2', 'Available'),
    orb( 'LH.D.3', 'Available', 860, -740),
    orb( 'LH.E.1', 'Purchased', 680, -520),
    orb( 'LH.E.2', 'Available'),
    orb( 'LH.E.3', 'Available'),
    orb( 'LH.E.4', 'Available'),
    orb( 'LH.E.5', 'Available', 1000, -380)
];

export var lightHookEdges = [
    edge('LH', 'LH.A.1', "En", true),
    edge('LH', 'LH.B.1', "En"),
    edge('LH.B.1', 'LH.B.2', "En"),
    edge('LH.B.2', 'LH.B.3', "En"),
    edge('LH.B.3', 'LH.B.4', "En"),
    edge('LH.B.4', 'LH.B.5', "En"),
    edge('LH', 'LH.C.1', "En"),
    edge('LH.C.1', 'LH.C.2', "En"),
    edge('LH.C.2', 'LH.C.3', "En"),
    edge('LH.C.3', 'LH.C.4', "En"),
    edge('LH.C.4', 'LH.C.5', "En"),
    edge('LH.C.5', 'LH.C.6', "En"),
    edge('LH', 'LH.D.1', "En"),
    edge('LH.D.1', 'LH.D.2', "En"),
    edge('LH.D.2', 'LH.D.3', "En"),
    edge('LH.D.3', 'LH.D.4', "En"),
    edge('LH', 'LH.E.1', "En"),
    edge('LH.E.1', 'LH.E.2', "En"),
    edge('LH.E.2', 'LH.E.3', "En"),
    edge('LH.E.3', 'LH.E.4', "En"),
    edge('LH.E.4', 'LH.E.5', "En")
];
