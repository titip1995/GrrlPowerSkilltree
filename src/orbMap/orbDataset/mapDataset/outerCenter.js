/* jshint esversion:6 */
import {orb} from "./mapClasses/OrbClass.js";
// orb(ID, GROUP, X, Y, LABEL, IMAGE, DATA)
// any can be null except ID and GROUP.
import {edge} from "./mapClasses/EdgeClass.js";
// edge(from, to, status, dashed, arrow, straight, length)


// prep the edge list
var __outerCenterEdges = [];

// this will create edges from the connector's name
function createEdgeSet(pair) {
  var aN = pair.split("-")[0];
  var bN = pair.split("-")[1];
  var aE = edge(aN, pair, "En", true, false, true, "auto");
  var bE = edge(bN, pair, "En", true, false, true, "auto");
  __outerCenterEdges.push(aE);
  __outerCenterEdges.push(bE);
}

// Outer Center (CPurchasednects All the orbs)
var __outerCenterNodes = [
    // Life Support -> 6
    orb( 'LS-UK', 'Off'),
    orb( 'LS-MB', 'Off'),
    orb( 'LS-FLT', 'Off'),
    orb( 'LS-PPO', 'Off'),
    orb( 'LS-LH', 'Off'),
    orb( 'LS-CO', 'Off'),
    // Unknown -> 5
    orb( 'UK-MB', 'Off'),
    orb( 'UK-FLT', 'Off'),
    orb( 'UK-PPO', 'Off'),
    orb( 'UK-LH', 'Off'),
    orb( 'UK-CO', 'Off'),
    // Mr Bubble -> 4
    orb( 'MB-FLT', 'Off'),
    orb( 'MB-PPO', 'Off'),
    orb( 'MB-LH', 'Off', null, null, null, null, {
      notes:"Could this enable use of the lighthook within the bubble?"
    }),
    orb( 'MB-CO', 'Off'),
    // Flight -> 3
    orb( 'FLT-PPO', 'On', null, null, null, null, {
      notes:"Some sort of synergy? Why only between these two orbs? Could decrease kickback from the PPO orb."
    }),
    orb( 'FLT-LH', 'Off'),
    orb( 'FLT-CO', 'Off'),
    // PPO -> 2
    orb( 'PPO-LH', 'Off'),
    orb( 'PPO-CO', 'Off'),
    // Light Hook -> ComOrb
    orb( 'LH-CO', 'Off')
];

// this triggers creation of every edge for the inner center nodes.
for (var index in __outerCenterNodes) {
  createEdgeSet(__outerCenterNodes[index].id);
}


// this is the actual export of this module
export var outerCenterNodes = __outerCenterNodes;
export var outerCenterEdges = __outerCenterEdges;
