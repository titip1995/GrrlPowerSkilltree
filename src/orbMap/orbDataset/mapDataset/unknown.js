/* jshint esversion:6 */
import {orb} from "./mapClasses/OrbClass.js";
// orb(ID, GROUP, X, Y, LABEL, IMAGE, DATA)
// any can be null except ID and GROUP.
import {edge} from "./mapClasses/EdgeClass.js";
// edge(from, to, status, dashed, arrow, straight, length)

// Unknown Orb
export var UKNodes = [
    orb( 'UK', 'Orb', -300, 580, 'Still Unknown Orb', './images/Orbs/Unknown_Orb.png', {
      status:"Bringer of alien armadas",
      notes:"Probably some form of computer.",
      whatItDoes:"...What part of unknown do you not understand?"
    }),
    orb( 'UK.A.1', 'Available', -120, 750, null, null, {
      notes:"Could this unlock a screen or HUD?"
    }),
    orb( 'UK.B.1', 'Purchased', -280, 800),
    orb( 'UK.B.2', 'Purchased'),
    orb( 'UK.B.3', 'Available'),
    orb( 'UK.B.4', 'Available'),
    orb( 'UK.B.5', 'Available', -500, 1000),
    orb( 'UK.C.1', 'Purchased', -560, 560, null, null, {
      notes:"Could this path allow for telepathic control over the orbs? Prehaps this is what lets her move the orbs mentally."
    }),
    orb( 'UK.C.2', 'Available'),
    orb( 'UK.C.3', 'Available'),
    orb( 'UK.C.4', 'Available'),
    orb( 'UK.C.5', 'Available', -780, 780)
];
export var UKEdges = [
    edge('UK', 'UK.A.1', "En", true),
    edge('UK', 'UK.B.1', "En"),
    edge('UK.B.1', 'UK.B.2', "En"),
    edge('UK.B.2', 'UK.B.3', "En"),
    edge('UK.B.3', 'UK.B.4', "En"),
    edge('UK.B.4', 'UK.B.5', "En"),
    edge('UK', 'UK.C.1', "En"),
    edge('UK.C.1', 'UK.C.2', "En"),
    edge('UK.C.2', 'UK.C.3', "En"),
    edge('UK.C.3', 'UK.C.4', "En"),
    edge('UK.C.4', 'UK.C.5', "En")
];
