/* jshint esversion:6 */
import {orb} from "./mapClasses/OrbClass.js";
// orb(ID, GROUP, X, Y, LABEL, IMAGE, DATA)
// any can be null except ID and GROUP.
import {edge} from "./mapClasses/EdgeClass.js";
// edge(from, to, status, dashed, arrow, straight, length)


// Life Support Orb
export var LSNodes = [
    orb( 'LS', 'Orb', 300, 580, 'Life Support Orb', './images/Orbs/LifeSupport_Orb.png', {
      status:"Pine Fresh",
      notes:"Can it be used in a vacuum?",
      whatItDoes:{
        k:"ul",
        c:[
          "Created air underwater, can provide breathable air from water and air(that has insufficent O2 content)."
        ]
      }
    }),
    orb( 'LS.A.1', 'Purchased', 520, 540, null, null, {
      notes:"Could this allow it to create air in a vacuum?"
    }),
    orb( 'LS.B.1', 'Purchased', 460, 660, null, null, {
      notes:"Sents? Possibly range of effect."
    }),
    orb( 'LS.B.2', 'Available'),
    orb( 'LS.B.3', 'Available'),
    orb( 'LS.C.1', 'Available', 200, 760),
    orb( 'LS.C.2', 'Available'),
    orb( 'LS.C.3', 'Available'),
    orb( 'LS.C.4', 'Available', 500, 860)
];

export var LSEdges = [
    edge('LS', 'LS.A.1', "En", true),
    edge('LS', 'LS.B.1', "En"),
    edge('LS.B.1', 'LS.B.2', "En"),
    edge('LS.B.2', 'LS.B.3', "En"),
    edge('LS', 'LS.C.1', "En"),
    edge('LS.C.1', 'LS.C.2', "En"),
    edge('LS.C.2', 'LS.C.3', "En"),
    edge('LS.C.3', 'LS.C.4', "En")
];
