/* jshint esversion:6 */

// This file gethers all the files and passes them out as a single item.

import {comOrbNodes, comOrbEdges} from "./mapDataset/communications.js";
import {lightHookNodes, lightHookEdges} from "./mapDataset/lightHook.js";
import {PPONodes, PPOEdges} from "./mapDataset/weapons.js";
import {FLTNodes, FLTEdges} from "./mapDataset/propulsion.js";
import {MBNodes, MBEdges} from "./mapDataset/shields.js";
import {UKNodes, UKEdges} from "./mapDataset/unknown.js";
import {LSNodes, LSEdges} from "./mapDataset/life_support.js";
import {outerCenterNodes, outerCenterEdges} from "./mapDataset/outerCenter.js";
import {innerCenterNodes, innerCenterEdges} from "./mapDataset/innerCenter.js";
import {nodeGroupStyles} from "./orbStyles.js";


var orbNodes = [...comOrbNodes, ...lightHookNodes, ...PPONodes, ...FLTNodes, ...MBNodes, ...UKNodes, ...LSNodes];
var orbEdges = [...comOrbEdges, ...lightHookEdges, ...PPOEdges, ...FLTEdges, ...MBEdges, ...UKEdges, ...LSEdges];


// this combines all the lists into a single list for the map to use.
// this is how we maintain easy to read code!
var allNodes = [...orbNodes, ...outerCenterNodes, ...innerCenterNodes];
var allEdges = [...orbEdges, ...outerCenterEdges, ...innerCenterEdges];

export var orbDataset = {nodes: allNodes, edges: allEdges, nodeStyles: nodeGroupStyles};

// Ok, fine, I lied, there are two outputs.
import {absoluteDefaultMetadata, allGroupDefaultMetadata, generalSettings} from "./metadata_and_settings.js";
export var options = {'defaultMD': absoluteDefaultMetadata, 'groupMD':allGroupDefaultMetadata, 'settings':generalSettings}
