/* jshint esversion:6 */
// the group styles for the network

export var nodeGroupStyles = {
    UnAvailable: {
        shape: 'dot',
        color: {background:'#9300ff',border:'#750099'},
        font: {color:'black', size: 30},
        size: 20
    },
    Available: {
        shape: 'dot',
        color: {background:'#FE0000',border:'#800000'},
        font: {color:'black', size: 30},
        size: 20
    },
    Purchased: {
        shape: 'dot',
        color: {background:'#007F0E',border:'#073B0B'},
        font: {color:'black', size: 30},
        size: 20
    },
    On: {
        shape: 'dot',
        color: {background:'#007F0E',border:'#073B0B'},
        font: {color:'black', size: 30},
        size: 20
    },
    Off: {
        shape: 'dot',
        color: {background:'#FE0000',border:'#800000'},
        font: {color:'black', size: 30},
        size: 20
    },
    Disabled: {
        shape: 'dot',
        color: {background:'#FF00DC',border:'#4E0042'},
        font: {color:'black', size: 30},
        size: 20
    },
    VanishingPoint: {
        shape: 'dot',
        color: {background:'gray',border:'darkgray'},
        font: {color:'black', size: 30},
        size: 20
    },
    Hidden: {
        shape: 'dot',
        color: {background:'white',border:'white'},
        font: {color:'black', size: 30},
        size: 20
    },
    Orb: {
      shape: 'circularImage',
      size:50,
      font:{color:'black', size:'35', background:'#FFFFFF80'}
    },
    TrueCenter: {
      image:'./images/Orbs/PizzaPie.png',
      shape: 'image',
      color: {background:'#FFFFFF80',border:'#FFFFFF80'},
      shapeProperties: {
        useBorderWithImage:true
      },
      size:40
    }
};
